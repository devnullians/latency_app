This application measures round-trip audio latency on an iOS or Android device. 

Low round-trip audio latency is a strong indicator of how well any mobile device is optimized for professional audio. 
Lower latency confers significant benefits to users of all sorts of apps like games, augmented hearing apps, VOIP and other interactive apps.  
Read more: http://superpowered.com/latency

The core of this app is a cross-platform class under: Android/app/src/main/jni/latencyMeasurer.cpp


# Android app compilation

After cloning the repository, to compile the Android App, follow these steps:

Assuming you already have Android studio working and setup:

1. Note location of Android SDK: `/my/path/adt-bundle-xxxxxx/sdk`
2. Install and note location of Android NDK: `my/path/android-ndk-rrrrrr`
3. Open Android studio and select import gradle project. Point it to the `Android` directory.
4. In Android Studio open Project --> Gradle Properties --> local.properties. Set
```
sdk.dir=/my/path/adt-bundle-xxxxxx/sdk
ndk.dir=my/path/android-ndk-rrrr
```
5. Make Project

To run, connect device (unless already connected) select Run-> Run 'app' and select device from popup